import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	private static final long serialVersionUID = 3426940946811133635L;
	private static final int GRID_X = 25;
	private static final int GRID_Y = 25;
	private static final int INNER_CELL_SIZE = 50;
	private static final int TOTAL_COLUMNS = 10;
	private static final int TOTAL_ROWS = 11;   //Last row has only one cell
	public Random rand=new Random();
	public int x = -1;
	public int y = -1;
	public int mouseDownGridX = 0;
	public int mouseDownGridY = 0;
	public Color [][]mines=new Color[TOTAL_COLUMNS][TOTAL_ROWS];
	public Color[][] colorArray = new Color[TOTAL_COLUMNS][TOTAL_ROWS];
	public MyPanel() {   //This is the constructor... this code runs first to initialize
		if (INNER_CELL_SIZE + (new Random()).nextInt(1) < 1) {	//Use of "random" to prevent unwanted Eclipse warning
			throw new RuntimeException("INNER_CELL_SIZE must be positive!");
		}
		if (TOTAL_COLUMNS + (new Random()).nextInt(1) < 2) {	//Use of "random" to prevent unwanted Eclipse warning
			throw new RuntimeException("TOTAL_COLUMNS must be at least 2!");
		}
		if (TOTAL_ROWS + (new Random()).nextInt(1) < 3) {	//Use of "random" to prevent unwanted Eclipse warning
			throw new RuntimeException("TOTAL_ROWS must be at least 3!");
		}
		for (int x = 0; x < TOTAL_COLUMNS; x++) {   //Top row
			colorArray[x][0] = Color.LIGHT_GRAY;
		}
		for (int y = 0; y < TOTAL_ROWS; y++) {   //Left column
			colorArray[0][y] = Color.LIGHT_GRAY;
		}
		for (int x = 1; x < TOTAL_COLUMNS; x++) {   //The rest of the grid
			for (int y = 1; y < TOTAL_ROWS; y++) {
				colorArray[x][y] = Color.WHITE;
			}
		}
		setMines();
		minesCount();

	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		//Compute interior coordinates
		Insets myInsets = getInsets();
		int x1 = myInsets.left;
		int y1 = myInsets.top;
		int x2 = getWidth() - myInsets.right - 1;
		int y2 = getHeight() - myInsets.bottom - 1;
		int width = x2 - x1;
		int height = y2 - y1;

		//Paint the background
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(x1, y1, width + 1, height + 1);

		//Draw the grid minus the bottom row (which has only one cell)
		//By default, the grid will be 10x10 (see above: TOTAL_COLUMNS and TOTAL_ROWS) 
		g.setColor(Color.BLACK);
		for (int y = 0; y <= TOTAL_ROWS - 1; y++) {
			g.drawLine(x1 + GRID_X, y1 + GRID_Y + (y * (INNER_CELL_SIZE + 1)), x1 + GRID_X + ((INNER_CELL_SIZE + 1) * TOTAL_COLUMNS), y1 + GRID_Y + (y * (INNER_CELL_SIZE + 1)));
		}
		for (int x = 0; x <= TOTAL_COLUMNS; x++) {
			g.drawLine(x1 + GRID_X + (x * (INNER_CELL_SIZE + 1)), y1 + GRID_Y, x1 + GRID_X + (x * (INNER_CELL_SIZE + 1)), y1 + GRID_Y + ((INNER_CELL_SIZE + 1) * (TOTAL_ROWS - 1)));
		}

		//Draw an additional cell at the bottom left
		g.drawRect(x1 + GRID_X, y1 + GRID_Y + ((INNER_CELL_SIZE + 1) * (TOTAL_ROWS - 1)), INNER_CELL_SIZE + 1, INNER_CELL_SIZE + 1);

		//Paint cell colors
		for (int x = 0; x < TOTAL_COLUMNS; x++) {
			for (int y = 0; y < TOTAL_ROWS; y++) {
				if ((x == 0) || (y != TOTAL_ROWS - 1)) {
					Color c = colorArray[x][y];
					g.setColor(c);
					g.fillRect(x1 + GRID_X + (x * (INNER_CELL_SIZE + 1)) + 1, y1 + GRID_Y + (y * (INNER_CELL_SIZE + 1)) + 1, INNER_CELL_SIZE, INNER_CELL_SIZE);
					if(colorArray[x][y]==Color.GRAY)
					{
						colorArray[x][y]=Color.GRAY;
					}
				}
			}
		}
	}


	// This method helps to find the adjacent boxes that don't have a mine.
	// It is partially implemented since the verify hasn't been discussed in class
	// Verify that the coordinates in the parameters are valid.
	// Also verifies if there are any mines around the x,y coordinate
   
	//abre los cuadros sin bombas alrededor
	public void revealAdjacent(int x, int y){
		if((x>0) && (y>0) && (x<10) && (y<10) && colorArray[x][y] != Color.GRAY){
			if(mines[x][y] == Color.WHITE) {
				colorArray[x][y] = Color.GRAY;
				revealAdjacent(x-1, y);
				revealAdjacent(x+1, y);
				revealAdjacent(x, y-1);
				revealAdjacent(x, y+1);
				//revealAdjacent(x+1,y+1);
				//revealAdjacent(x-1,y-1);
				//revealAdjacent(x+1,y-1);
				//revealAdjacent(x-1,y+1);
			}
			colorArray[x][y] =Color.GRAY;
			numbers(mines[x][y],this,Main.myFrame,x,y);
		}
	}
//cuenta las minas alrededor
	public void minesCount() {
		int bombsCount=0;
		for(int x=1;x<=9;x++) {
			for(int y=1;y<=9;y++){
				if(mines[x][y]!=Color.BLACK) {
					if(x!=1 && mines[x-1][y].equals(Color.BLACK)) {
						bombsCount++;
					}
					if(x!=9 && mines[x+1][y].equals(Color.BLACK)) {
						bombsCount++;
					}
					if(y!=1 && mines[x][y-1].equals(Color.BLACK)) {
						bombsCount++;
					}
					if(y!=9 && mines[x][y+1].equals(Color.BLACK)) {
						bombsCount++;
					}
					if(x!=1 && y!=9 && mines[x-1][y+1].equals(Color.BLACK)) {
						bombsCount++;
					}
					if(x!=1 && y!=1 && mines[x-1][y-1].equals(Color.BLACK)) {
						bombsCount++;
					}
					if(x!=9 && y!=9 && mines[x+1][y+1].equals(Color.BLACK)) {
						bombsCount++;
					}
					if(x!=9 && y!=1 && mines[x+1][y-1].equals(Color.BLACK)) {
						bombsCount++;
					}

					switch(bombsCount) {
					case 1:
						mines[x][y]=Color.BLUE;
					
						break;

					case 2:
						mines[x][y]=Color.GREEN;
					
						break;

					case 3:
						mines[x][y]=Color.YELLOW;
						
						break;

					case 4:
						mines[x][y]=Color.RED;
						
						break;
					case 5:
						mines[x][y]=Color.MAGENTA;
						
						break;
					case 6:
						mines[x][y]=Color.CYAN;
					
						break;
					case 7:
						mines[x][y]=Color.ORANGE;
					
						break;
					case 8:
						mines[x][y]=Color.PINK;
						
						break;
					}
					bombsCount=0;
				}
			}
		}



	}
//
	public void numbers(Color x,MyPanel myPanel,JFrame jFrame,int i,int j) {
		JLabel label1= new JLabel();
		if(x.equals(Color.BLUE)) {
			int u=1;
			label1.setText(" "+u);
			label1.setForeground(Color.BLUE);
		}
		else if(x.equals(Color.GREEN)) {
			int u=2;
			label1.setText(" "+u);
			label1.setForeground(Color.GREEN);
		}
		else if(x.equals(Color.YELLOW)) {
			int u=3;
			label1.setText(" "+u);
			label1.setForeground(Color.YELLOW);
		}
		else if(x.equals(Color.RED)) {
			int u=4;
			label1.setText(" "+u);
			label1.setForeground(Color.RED);
		}
		else if(x.equals(Color.MAGENTA)) {
			int u=5;
			label1.setText(" "+u);
			label1.setForeground(Color.RED);	
		}
		else if(x.equals(Color.CYAN)) {
			int u=6;
			label1.setText(" "+u);
			label1.setForeground(Color.RED);
		}
		else if(x.equals(Color.ORANGE)) {
			int u=7;
			label1.setText(" "+u);
			label1.setForeground(Color.RED);
		}
		else if(x.equals(Color.PINK)) {
			int u=8;
			label1.setText(" "+u);
			label1.setForeground(Color.RED);
		}
		label1.setBounds((i*50)+46,(j*50)-443, 1000, 1000);
		myPanel.add(label1);
		myPanel.repaint();
		jFrame.add(myPanel);
		jFrame.repaint();
	}

	public void setMines(){
		int m=0;

		for(int x=1;x<=9;x++) {
			for(int y=1;y<=9;y++){
				mines[x][y]=Color.WHITE;
			}
		}

		while(m<10){
			int x=rand.nextInt(8)+1;
			int y=rand.nextInt(8)+1;
			if(mines[x][y] != Color.BLACK){
				mines[x][y] = Color.BLACK;
				m++;

			}

		}
	}

	public int getGridX(int x, int y) {
		Insets myInsets = getInsets();
		int x1 = myInsets.left;
		int y1 = myInsets.top;
		x = x - x1 - GRID_X;
		y = y - y1 - GRID_Y;
		if (x < 0) {   //To the left of the grid
			return -1;
		}
		if (y < 0) {   //Above the grid
			return -1;
		}
		if ((x % (INNER_CELL_SIZE + 1) == 0) || (y % (INNER_CELL_SIZE + 1) == 0)) {   //Coordinate is at an edge; not inside a cell
			return -1;
		}
		x = x / (INNER_CELL_SIZE + 1);
		y = y / (INNER_CELL_SIZE + 1);
		if (x == 0 && y == TOTAL_ROWS - 1) {    //The lower left extra cell
			return x;
		}
		if (x < 0 || x > TOTAL_COLUMNS - 1 || y < 0 || y > TOTAL_ROWS - 2) {   //Outside the rest of the grid
			return -1;
		}
		return x;
	}
	public int getGridY(int x, int y) {
		Insets myInsets = getInsets();
		int x1 = myInsets.left;
		int y1 = myInsets.top;
		x = x - x1 - GRID_X;
		y = y - y1 - GRID_Y;
		if (x < 0) {   //To the left of the grid
			return -1;
		}
		if (y < 0) {   //Above the grid
			return -1;
		}
		if ((x % (INNER_CELL_SIZE + 1) == 0) || (y % (INNER_CELL_SIZE + 1) == 0)) {   //Coordinate is at an edge; not inside a cell
			return -1;
		}
		x = x / (INNER_CELL_SIZE + 1);
		y = y / (INNER_CELL_SIZE + 1);
		if (x == 0 && y == TOTAL_ROWS - 1) {    //The lower left extra cell
			return y;
		}
		if (x < 0 || x > TOTAL_COLUMNS - 1 || y < 0 || y > TOTAL_ROWS - 2) {   //Outside the rest of the grid
			return -1;
		}
		return y;
	}
}