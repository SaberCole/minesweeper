
import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class MyMouseAdapter extends MouseAdapter {
	private Random generator = new Random();


	public void mousePressed(MouseEvent e) {
		switch (e.getButton()) {
		case 1:		//Left mouse button
			Component c = e.getComponent();
			while (!(c instanceof JFrame)) {
				c = c.getParent();
				if (c == null) {
					return;
				}
			}
			JFrame myFrame = (JFrame) c;
			MyPanel myPanel = (MyPanel) myFrame.getContentPane().getComponent(0);
			Insets myInsets = myFrame.getInsets();
			int x1 = myInsets.left;
			int y1 = myInsets.top;
			e.translatePoint(-x1, -y1);
			int x = e.getX();
			int y = e.getY();
			myPanel.x = x;
			myPanel.y = y;
			myPanel.mouseDownGridX = myPanel.getGridX(x, y);
			myPanel.mouseDownGridY = myPanel.getGridY(x, y);
			myPanel.repaint();
			break;
		case 3:
			c = e.getComponent();
			while (!(c instanceof JFrame)) {
				c = c.getParent();
				if (c == null) {
					return;
				}
			}
			myFrame = (JFrame) c;
			myPanel = (MyPanel) myFrame.getContentPane().getComponent(0);
			myInsets = myFrame.getInsets();
			x1 = myInsets.left;
			y1 = myInsets.top;
			e.translatePoint(-x1, -y1);
			x = e.getX();
			y = e.getY();
			myPanel.x = x;
			myPanel.y = y;
			myPanel.mouseDownGridX = myPanel.getGridX(x, y);
			myPanel.mouseDownGridY = myPanel.getGridY(x, y);
			myPanel.repaint();

			break;

		default:    //Some other button (2 = Middle mouse button, etc.)
			//Do nothing
			break;
		}
	}

	public void mouseReleased(MouseEvent e) {

		switch (e.getButton()) {
		case 1:		//Left mouse button
			Component c = e.getComponent();
			while (!(c instanceof JFrame)) {
				c = c.getParent();
				if (c == null) {
					return;
				}
			}
			JFrame myFrame = (JFrame)c;
			MyPanel myPanel = (MyPanel) myFrame.getContentPane().getComponent(0);  //Can also loop among components to find MyPanel
			Insets myInsets = myFrame.getInsets();
			int x1 = myInsets.left;
			int y1 = myInsets.top;
			e.translatePoint(-x1, -y1);
			int x = e.getX();
			int y = e.getY();
			myPanel.x = x;
			myPanel.y = y;
			int gridX = myPanel.getGridX(x, y);
			int gridY = myPanel.getGridY(x, y);
			if ((myPanel.mouseDownGridX == -1) || (myPanel.mouseDownGridY == -1)) {
				//Had pressed outside
				//Do nothing
			} else {
				if ((gridX == -1) || (gridY == -1)) {
					//Is releasing outside
					//Do nothing
				} else {
					if ((myPanel.mouseDownGridX != gridX) || (myPanel.mouseDownGridY != gridY)) {
						//Released the mouse button on a different cell where it was pressed
						//Do nothing

					} else {
						//Released the mouse button on the same cell where it was pressed
						if ((gridX == 0) || (gridY == 0)) {
							//On the left column and on the top row... do nothing
						} else {
							//On the grid other than on the left column and on the top row:
						

							if(myPanel.mines[myPanel.mouseDownGridX][myPanel.mouseDownGridY]==Color.BLACK){
								for(int i=1;i<=9;i++){
									for(int j=1;j<=9;j++){
										if(myPanel.mines[i][j]==Color.BLACK){
											myPanel.colorArray[i][j]=Color.BLACK;
											myPanel.repaint();
										}
									}
								}
								if(gameOver()) {
									myFrame.dispose();
									Main.main(null);
								}else {
									myFrame.dispose();
								}
							}
							/*else if(myPanel.colorArray[myPanel.mouseDownGridX][myPanel.mouseDownGridY]==Color.GRAY){//En este if se estaba intentando contar los cuadrados grises, pero al llamar al metodo win() no contaba todos los cuadrados grises bien y dejaba cuadrados blancos..
								int selectedSquares=0;
								int i=1;
								int j=1;
								for( i=1;i<=9;i++){
									for( j=1;j<=9;j++){
										if(myPanel.colorArray[i][j]==Color.GRAY){
											
											selectedSquares++;
											
										}
									}
								}
								if(selectedSquares==71){
									win();
									
								}else if(win()){
									myFrame.dispose();
									Main.main(null);
								}else {
									myFrame.dispose();
								}
							}*/

							else if(myPanel.mines[myPanel.mouseDownGridX][myPanel.mouseDownGridY]!=Color.WHITE&&
									(myPanel.colorArray[myPanel.mouseDownGridX][myPanel.mouseDownGridY]==Color.WHITE)) {
								Color n = myPanel.mines[myPanel.mouseDownGridX][myPanel.mouseDownGridY];
								myPanel.colorArray[myPanel.mouseDownGridX][myPanel.mouseDownGridY] = Color.GRAY;
								myPanel.repaint();
								myPanel.numbers(n, myPanel,myFrame,myPanel.mouseDownGridX,myPanel.mouseDownGridY);


							}
							else{
								myPanel.revealAdjacent(myPanel.mouseDownGridX, myPanel.mouseDownGridY);
							}
						}
					}
				}
			}
			myPanel.repaint();
			break;

		case 3:		
			c = e.getComponent();
			while (!(c instanceof JFrame)) {
				c = c.getParent();
				if (c == null) {
					return;
				}
			}
			myFrame = (JFrame)c;
			myPanel = (MyPanel) myFrame.getContentPane().getComponent(0);  //Can also loop among components to find MyPanel
			myInsets = myFrame.getInsets();
			x1 = myInsets.left;
			y1 = myInsets.top;
			e.translatePoint(-x1, -y1);
			x = e.getX();
			y = e.getY();
			myPanel.x = x;
			myPanel.y = y;
			gridX = myPanel.getGridX(x, y);
			gridY = myPanel.getGridY(x, y);
			if ((myPanel.mouseDownGridX == -1) || (myPanel.mouseDownGridY == -1)) {
				//Had pressed outside
				//Do nothing
			} else {
				if ((gridX == -1) || (gridY == -1)) {
					//Is releasing outside
					//Do nothing
				} else {
					if ((myPanel.mouseDownGridX != gridX) || (myPanel.mouseDownGridY != gridY)) {
						//Released the mouse button on a different cell where it was pressed
						//Do nothing
					} else {
						//Released the mouse button on the same cell where it was pressed
						if ((gridX == 0) || (gridY == 0)) {
							//On the left column and on the top row... do nothing
						} else {
							//On the grid other than on the left column and on the top row:
							Color newColor =new Color(0xFF0000) ;
							if(myPanel.colorArray[myPanel.mouseDownGridX][myPanel.mouseDownGridY].equals(Color.GRAY)) {
								
							}
							else if(myPanel.colorArray[myPanel.mouseDownGridX][myPanel.mouseDownGridY].equals(new Color(0xFF0000))) {
								newColor=Color.WHITE;
								myPanel.colorArray[myPanel.mouseDownGridX][myPanel.mouseDownGridY] = newColor;
								myPanel.repaint();
							}
							
							else{
								myPanel.colorArray[myPanel.mouseDownGridX][myPanel.mouseDownGridY] = newColor;
								myPanel.repaint();
							}

						}
					}
				}
			}

			break;
		default:    //Some other button (2 = Middle mouse button, etc.)
			//Do nothing
			break;
		}
	}

	public boolean gameOver() {
		JOptionPane.showMessageDialog(null,"You lost","Minesweeper Christopher",JOptionPane.PLAIN_MESSAGE);

		return (JOptionPane.showConfirmDialog(null,"Try Again?","Minesweeper",JOptionPane.PLAIN_MESSAGE)==0);
	}
	/*public boolean win(){//Metodo win() que no funciono con el counter selectedSquares
		JOptionPane.showMessageDialog(null, "You won","Minesweeper",JOptionPane.PLAIN_MESSAGE);
		return (JOptionPane.showConfirmDialog(null,"Try Again?","Minesweeper",JOptionPane.PLAIN_MESSAGE)==0);
	}*/
}
